import React from 'react';
import { createBottomTabNavigator } from "react-navigation";
import Home from "./Home";
import Booking from './Booking';
import Tutorial from './Tutorial';
import History from './History';
import Profile from './Profile';
import Login from './Login';
import BookingButton from './button/BookingButton';
import Icon from 'react-native-vector-icons/MaterialIcons';

export const MainMenu = createBottomTabNavigator({
    Home: {
      screen: Home,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => {
          return <Icon name="home" color={tintColor} size={30}/>
        },
      })
    },
    Tutorial: {
      screen: Tutorial,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => {
          return <Icon name="airplay" color={tintColor} size={30} />
        }
      })
    },
    Booking: {
      screen: Booking,
      navigationOptions: () => ({
        tabBarIcon: <BookingButton />
      })
    },
    History: {
      screen: History,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => {
          return <Icon name="list" color={tintColor} size={30} />
        }
      })
    },
    Profile: {
      screen: Profile,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => {
          return <Icon name="person" color={tintColor} size={30} />
        }
      })
    },
    Login: {
      screen: Login,
      navigationOptions: () => ({
        tabBarVisible: false
      })
    }
}, {
    tabBarOptions: {
      showLabel: false,
      activeTintColor: '#DA251D',
      style: {
        borderTopWidth: 0,
        elevation: 30,
      }
    },
}, {
    initialRouteName: 'Home'
})