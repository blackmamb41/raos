import React from 'react';
import { View, Text, AsyncStorage, StyleSheet } from 'react-native';
import {
    Container,
    Content,
    Card,
    CardItem,
    Button,
    Icon,
    Spinner,
    Form,
    Textarea
} from 'native-base'
import { Grid, Row, Col } from 'react-native-easy-grid'
import DatePicker from 'react-native-datepicker'
import { SelectMultipleButton, SelectMultipleGroupButton } from 'react-native-selectmultiple-button'
import Axios from 'axios';
import { urlApi } from './utils/Server';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { unserialize } from 'locutus/php/var';
import { MaterialDialog } from 'react-native-material-dialog';
import { material } from 'react-native-typography'

const styling = StyleSheet.create({
    buttonJam: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20,
        width: '20%',
        borderRadius: 50
    },
})

const defaultClock = [
    { value: 7, displayValue: 7 },
    { value: 8, displayValue: 8 },
    { value: 9, displayValue: 9 },
    { value: 10, displayValue: 10 },
    { value: 11, displayValue: 11 },
    { value: 12, displayValue: 12 },
    { value: 13, displayValue: 13 },
    { value: 14, displayValue: 14 },
    { value: 15, displayValue: 15 },
    { value: 16, displayValue: 16 },
    { value: 17, displayValue: 17 },
    { value: 18, displayValue: 18 }
]
const dataset = [7,8,9,10,11,12,13,14,15,16,17]
const indexJam = []

class ClockButton extends React.Component {
    render() {
        return (
            <SelectMultipleGroupButton
                defaultSelectedIndex={indexJam}
                multiple={true}
                containerViewStyle={{ flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'center' }}
                highLightStyle={{
                    borderColor: "gray",
                    backgroundColor: "transparent",
                    textColor: "gray",
                    borderTintColor: "green",
                    backgroundTintColor: "green",
                    textTintColor: "white"
                }}
                buttonViewStyle={{ width: 80, height: 80, borderRadius: 40 }}
                onSelectedValuesChange={selected => this.newCheckBooking(selected)}
                group={defaultClock}
            />
        )
    }
}

export default class Booking extends React.Component {
    static navigationOptions = {
        title: 'Booking',
        headerTitleStyle: {
            textAlign: 'center',
            flex: 1,
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            tanggal: null,
            loading: true,
            initialClock: [
                { jam: 6, status: 'empty'},
                { jam: 7, status: 'empty'},
                { jam: 8, status: 'empty'},
                { jam: 9, status: 'empty'},
                { jam: 10, status: 'empty'},
                { jam: 11, status: 'empty'},
                { jam: 12, status: 'empty'},
                { jam: 13, status: 'empty'},
                { jam: 14, status: 'empty'},
                { jam: 15, status: 'empty'},
                { jam: 16, status: 'empty'},
                { jam: 17, status: 'empty'},
                { jam: 18, status: 'empty'},
            ],
            dataJam: [],
            pilihJam: [],
            perihal: null,
            dataEmployee: {},
            nikEmployee: null,
            singleData: {},
            loading: false,
            error: false,
            statusBook: false
        }
        this.setDate = this.setDate.bind(this);
    }

    async componentDidMount() {
        await AsyncStorage.getItem('employee', (error, data) => {
            jsonData = JSON.parse(data)
            this.setState({
                dataEmployee: jsonData,
                nikEmployee: jsonData.nik
            })
        })
        this.setState({loading: false})
    }

    setDate(tglDipilih) {
        this._deniedSelectJam()
        this.setState({
            tanggal: tglDipilih,
            loading: !this.state.loading
        })
        this.getDataCheckBooking()
        console.log(this.state.tanggal)
    }

    getDateNow(date) {
        var now = new Date(date),
            month = '' + (now.getMonth() + 1),
            day = '' + (now.getDate()),
            year = now.getFullYear();
        if (month.length < 2) month = '0' + month
        if (day.length < 2) day = '0' + day

        return [year, month, day].join('/');
    }

    getTimeNow(date) {
        let now = new Date(date),
            hh = '' + (now.getHours()),
            mm = '' + (now.getMinutes());
        if(hh.length < 2) hh = '0' + hh
        if(mm.length < 2) mm = '0' + mm

        return [hh,mm].join(':');
    }

    getDataCheckBooking = () => {
        Axios.get(urlApi+'/booking/check/tanggal/'+this.state.tanggal)
            .then(response => {
                this.setState({dataJam: response.data.data})
                this.setState({loading: !this.state.loading})
                this.renderJamButton()
            }).catch(error => {
                console.log(error)
            })
    }

    _deniedSelectJam() {
        this.state.pilihJam.length = 0
    }

    _onChangeSelectJam(selectedJam) {
        this.setState({
            pilihJam: selectedJam
        })
    }

    _bookNow = () => {
        if(this.state.pilihJam.length == 0) {
            console.log('Time Not Selected')
        }else if(this.state.perihal == null) {
            console.log('Perihal Tidak diisi')
        }else{
            Axios.post(urlApi+'/booking/add', {
                perihal: this.state.perihal,
                tanggal: this.state.tanggal,
                jam: this.state.pilihJam,
                employees_nik: this.state.nikEmployee
            }).then(response => {
                console.log('Modal Success')
            }).catch(error => {
                console.log(error)
            })
        }
    }

    renderJamButton() {
        if(this.state.tanggal == null) {
            return(<Text>Please Choose Date First</Text>)
        }else if(this.state.loading == true) {
            return(
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <Spinner color='green' />
                </View>
            )
        }else{
            const jam = [8,9,10,11,12,13,14,15,16,17,18]
            let margeJamBooked = []
            const BookedJam = this.state.dataJam.map((bookingJam) => {
                unserialize(bookingJam.jam).map(dataJam => {
                    margeJamBooked.push(dataJam)
                })
            })
            const jamExc = jam.filter((list) => {
                return !margeJamBooked.includes(list)
            })
            const jamKosong = []
            jamExc.map((data) => {
                jamKosong.push({value: data, displayValue: data})
            })
            return(
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <Icon name='time' />
                    <SelectMultipleGroupButton
                        containerViewStyle={{flexDirection: 'row'}}
                        highLightStyle={{
                            borderColor: "gray",
                            backgroundColor: "transparent",
                            textColor: "gray",
                            borderTintColor: "green",
                            backgroundTintColor: "green",
                            textTintColor: "white"
                        }}
                        buttonViewStyle={{ width: 60, height: 60, borderRadius: 30}}
                        group={jamKosong}
                        onSelectedValuesChange={selectedJam => this._onChangeSelectJam(selectedJam)}
                    />
                    <Text>KEPENTINGAN</Text>
                    <Form style={{width: '100%', borderRadius: 5}}>
                        <Textarea rowSpan={5} bordered onChangeText={(dataPerihal) => this.setState({perihal: dataPerihal})} />
                    </Form>
                </View>
            )
        }
    }

    actionButtonRender() {
        if(this.state.tanggal == null) {
            return (
                <View>
                </View>
            )
        }else{
            return(
                <View>
                    <Row style={{marginVertical: '1%'}}>
                        <Button primary style={{width: '95%'}}><Icon name='person' /><Text style={{flex: 1, flexDirection: 'row', textAlign: 'center', color: '#fff'}}>AUDIENCE</Text><Icon name='person' /></Button>
                    </Row>
                    <Row style={{marginVertical: '1%'}}>
                        <Button success style={{width: '95%'}} onPress={() => {
                            if(this.state.pilihJam.length == 0) {
                                this.setState({error: true})
                            }else{
                                this.setState({statusBook: true})
                            }
                        }}><Icon name='save' /><Text style={{flex: 1, flexDirection: 'row', textAlign: 'center', color: '#fff'}}>BOOK</Text><Icon name='save' /></Button>
                    </Row>
                    <Row style={{marginVertical: '1%'}}>
                        <Button danger style={{width: '95%'}}><Icon name='refresh' /><Text style={{flex: 1, flexDirection: 'row', textAlign: 'center', color: '#fff'}}>RESET</Text><Icon name='refresh' /></Button>
                    </Row>
                </View>
            )
        }
    }

    render() {
        return(
            <Container style={{backgroundColor: '#dfe6e9'}}>
                <MaterialDialog
                    visible={this.state.error}
                    title={'DISALLOWED'}
                    okLabel="OK"
                    onOk={() => {
                        this.setState({ error: false });
                    }}
                    cancelLabel="DISCARD"
                    onCancel={() => {
                        this.setState({ error: false });
                    }}
                >
                    <Text style={material.subheading}>Anda Belum Pilih Jam</Text>
                </MaterialDialog>
                <MaterialDialog
                    visible={this.state.statusBook}
                    title={'SAVE'}
                    okLabel="BOOK NOW"
                    onOk={this._bookNow}
                    cancelLabel="DISCARD"
                    onCancel={() => {
                        this.setState({ statusBook: false });
                    }}
                >
                    <Text style={material.subheading}>Anda Belum Pilih Jam</Text>
                </MaterialDialog>
                <ScrollView>
                    <Content style={{width: '100%', margin: '3%'}}>
                        <Grid>
                            <Row>
                                <Card>
                                    <CardItem header style={{flexDirection: "row", justifyContent: "center"}}>
                                        <DatePicker
                                            style={{width: '94%'}}
                                            date={this.state.tanggal}
                                            mode='date'
                                            placeholder='Pilih Tanggal'
                                            format='YYYY-MM-DD'
                                            minDate={this.getDateNow(Date.now)}
                                            maxDate='2099-10-10'
                                            confirmBtnText='Pilih'
                                            onDateChange={this.setDate}
                                            customStyles={{
                                                dateInput: {
                                                    borderRadius: 5,
                                                }
                                            }}
                                        />
                                    </CardItem>
                                    <CardItem style={{flexDirection: "row", justifyContent: "center"}}>
                                        {this.renderJamButton()}
                                    </CardItem>
                                </Card>
                            </Row>
                            {this.actionButtonRender()}
                        </Grid>
                        
                    </Content>
                </ScrollView>
            </Container>
        )
    }
}