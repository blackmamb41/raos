import React from 'react';
import { Platform, StatusBar, StyleSheet, View, Text } from 'react-native';
import { createAppContainer, createBottomTabNavigator, createStackNavigator, createSwitchNavigator } from "react-navigation";
import Home from "./Home";
import Booking from './Booking';
import Tutorial from './Tutorial';
import History from './History';
import Profile from './Profile';
import Login from './Login';
import BookingButton from './button/BookingButton';
import Icon from 'react-native-vector-icons/MaterialIcons';

const HomeMenu = createStackNavigator({
  Home: {
    screen: Home
  }
})

const TutorialMenu = createStackNavigator({
  Tutorial: {
    screen: Tutorial
  }
})

const BookingMenu = createStackNavigator({
  Booking: {
    screen: Booking
  }
})

const HistoryMenu = createStackNavigator({
  History: {
    screen: History
  }
})

const ProfileMenu = createStackNavigator({
  Profile: {
    screen: Profile
  }
})

const TabMenu = createBottomTabNavigator({
  Home: {
    screen: HomeMenu,
    navigationOptions: () => ({
      tabBarLabel: 'Home',
      tabBarIcon: ({tintColor}) => {
        return <Icon name="home" color={tintColor} size={30}/>
      },
    })
  },
  Tutorial: {
    screen: TutorialMenu,
    navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => {
        return <Icon name="airplay" color={tintColor} size={30} />
      }
    })
  },
  Booking: {
    screen: BookingMenu,
    navigationOptions: () => ({
      tabBarIcon: <BookingButton />
    })
  },
  History: {
    screen: HistoryMenu,
    navigationOptions: () => ({
      tabBarLabel: 'History',
      tabBarIcon: ({tintColor}) => {
        return <Icon name="list" color={tintColor} size={30} />
      }
    })
  },
  Profile: {
    screen: ProfileMenu,
    navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => {
        return <Icon name="person" color={tintColor} size={30} />
      }
    })
  },
}, {
  tabBarOptions: {
    showLabel: false,
    activeTintColor: '#DA251D',
    style: {
      borderTopWidth: 0,
      elevation: 30,
    }
  },
})

const RootRoute = createStackNavigator({
  Home: {
    screen: TabMenu,
    navigationOptions: {
      mode: 'modal',
      header: null,
      headerMode: 'none',
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      mode: 'modal',
      header: null,
      headerMode: 'none',
    }
  }
}, {
  initialRouteName: 'Login'
})

export default createAppContainer(RootRoute)