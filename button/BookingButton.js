import React from 'react'
import {TouchableHighlight, View, TouchableNativeFeedback} from 'react-native'
import Icon from '@expo/vector-icons/MaterialIcons'
import Ripple from 'react-native-material-ripple'
import { StackActions, NavigationActions, withNavigation } from 'react-navigation'

class BookingButton extends React.Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    goToBooking = () => {
        this.props.navigation.navigate('Booking')
    }
    render() {
        return(
            <View style={{
                // position: 'absolute',
                alignItems: 'center'
            }}>
                <Ripple
                    onPress={this.goToBooking}
                    rippleColor="rgb(237, 253, 253)"
                >
                    <TouchableHighlight
                        underlayColor="#fefe"
                        style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: 64,
                            height: 64,
                            borderRadius: 64 / 2,
                            backgroundColor: "#DA251D",
                            bottom: '12%'
                        }}
                    >
                        <Icon name="add" size={24} color="#fff"/>
                    </TouchableHighlight>
                </Ripple>
            </View>
        )
    }
}

export default withNavigation(BookingButton)