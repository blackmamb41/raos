import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, AsyncStorage } from 'react-native';
import Axios from 'axios'
import { Grid, Row, Col } from 'react-native-easy-grid'
import { Container, Header, Content, Form, Item, Input, Label, Icon, Button, Spinner } from 'native-base';
import GradientButton from 'react-native-gradient-buttons';
import { urlApi } from './utils/Server'
import { LinearGradient } from 'expo';
import { MaterialDialog } from 'react-native-material-dialog';
import { NavigationEvents, StackActions, NavigationActions } from 'react-navigation';

const style = StyleSheet.create({
    bgLogin: {
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    }
})

export default class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            nik: '',
            password: '',
            loading: false,
            error: false,
            errorMessage: '',
            ip: null
        }
    }

    componentWillMount() {
        Axios.get(urlApi+'/ip').then(response => {
            this.setState({ip: response.data.ip})
        });
        AsyncStorage.getItem('employee', (error, data) => {
            console.log(data)
            if(data != null) {
                this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Home'})]}))
            }
        })
    }

    loginAction = () => {
        const navigate = this.props.navigation
        this.setState({loading: true})
        Axios.post(urlApi+'/action/login', {
            nik: this.state.nik,
            password: this.state.password
        }).then(response => {
            if(response.status == 401) {
                this.setState({
                    loading: false,
                    error: true,
                    errorMessage: 'NIK atau Password anda Salah'
                })
            }else if(response.status == 200) {
                this.getEmployee(this.state.nik)
                    .then(data => {
                        let employeeData = {
                            name: data.name,
                            division: data.division,
                            position: data.position,
                            photo: data.photo,
                            expired: data.expired,
                            ip: this.state.ip,
                            nik: this.state.nik
                        }
                        AsyncStorage.setItem('employee', JSON.stringify(employeeData))
                        this.props.navigation.reset([NavigationActions.navigate({routeName: 'Home'})])
                        // this.props.navigation.dispatch(StackActions.reset({routeName: 'Home'}))
                    })
                this.setState({
                    loading: false
                })
            }
        }).catch(error => {
            console.log(error)
        })
        // console.log(this.state.nik);
        // console.log(this.state.password);
    }

    getEmployee(nik) {
        return Axios.get(urlApi+'/employee/'+nik)
            .then(response => {
                return response.data
            })
    }

    renderLoadingOrNot() {
        if(this.state.error == false) {
            return (<Text style={{color: '#fff', textAlign: 'center', fontSize: 18, marginTop: '3%', marginBottom: '3%'}}>LOGIN</Text>)
        }else{
            return (<Spinner color='#fff' />)
        }
    }

    render() {
        return (
            <View style={style.bgLogin}>
                <Grid>
                    <Row size={3} style={{flex: 1, flexDirection: 'column-reverse', alignItems: 'center'}}>
                        <Image source={require('./assets/images/telkomsel.png')} style={{width: 150, height: 150}}/>
                    </Row>
                    <Row size={2} style={{marginTop: '10%'}}>
                        <Container>
                            <Content>
                                <Item style={{marginLeft: '15%', marginRight: '15%'}}>
                                    <Icon name='people' style={{color: 'grey'}}/>
                                    <Input keyboardType='numeric' placeholder='NIK' onChangeText={(nik) => this.setState({nik})} />
                                </Item>
                                <Text>{"\n"}</Text>
                                <Item style={{marginLeft: '15%', marginRight: '15%'}}>
                                    <Icon name='key' style={{color: 'grey'}}/>
                                    <Input secureTextEntry={true} placeholder='Password' onChangeText={(password) => this.setState({password})} />
                                </Item>
                                <TouchableOpacity onPress={this.loginAction} style={{marginLeft: '12%', marginRight: '12%', marginTop: '10%', borderRadius: 30, height: 30}}>
                                    <LinearGradient
                                            start={[0, 0.5]}
                                            end={[1, 0.5]}
                                            colors={['#DA251D', '#1f1c18']}
                                            style={{borderRadius: 30}}
                                        >
                                    {this.renderLoadingOrNot()}
                                    </LinearGradient>
                                </TouchableOpacity>
                                <MaterialDialog
                                    title='Error'
                                    visible={this.state.error}
                                    onCancel={() => this.setState({ visible: false })}
                                >
                                    <Text>{this.state.errorMessage}</Text>
                                </MaterialDialog>
                                <Text>{'\n'}</Text>
                                <Text style={{color: 'red', flex: 1, textAlign: 'center'}}>Lupa Password ?</Text>
                            </Content>
                        </Container>
                    </Row>
                </Grid>
                <Text style={{justifyContent: 'center', textAlign: 'center', color: '#000', fontSize: 15}}>{'\u00A9'} IT Support Jawa Barat {'\u00A9'}</Text>
            </View>
        )
    }
}