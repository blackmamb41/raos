import React from "react";
import { View, Text, AsyncStorage, TouchableOpacity} from 'react-native';
import {
    Container,
    Content,
    Card,
    CardItem,
    Thumbnail,
    Body,
    Badge,
    Icon
} from 'native-base'
import { Grid, Row, Col } from 'react-native-easy-grid'
import Axios from "axios";
import { urlApi } from "./utils/Server";
import { unserialize } from "locutus/php/var";

export default class History extends React.Component {
    static navigationOptions = {
        title: 'History',
        headerTitleStyle: {
            textAlign: 'center',
            flex: 1,
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            history: [],
            nik: '',
            loading: '',
        }
    }

    async componentDidMount() {
        await AsyncStorage.getItem('employee', (error, data) => {
            let employee = JSON.parse(data)
            console.log(employee.nik)
            this.setState({nik: employee.nik})
        })

        await Axios.get(urlApi+'/employee/'+this.state.nik+'/history')
            .then(response => {
                this.setState({history: response.data})
            }).catch(error => {
                console.log(error)
            })
    }

    historyData = () => {
        dataHistory = this.state.history
        const forHistory = dataHistory.map((data) => {
            console.log(unserialize(data.jam))
            if(data.status == 'pending') {
                return (
                    <TouchableOpacity onPress={() => {
                        const historyUser = {
                            tiket: data.tiket,
                            jam: data.jam,
                            tanggal: data.tanggal
                        }
                        AsyncStorage.setItem('getHistoryItem', JSON.stringify(historyUser))
                        this.props.navigation.navigate('Tiket')
                    }}>
                        <Card>
                            <CardItem>
                                <Thumbnail source={require('./assets/images/skyline.png')} />
                                <Body style={{marginLeft: '10%', marginTop: '2%'}}>
                                    <Badge warning><Text style={{color: '#fff'}}>{data.status}</Text></Badge>
                                    <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                        <Icon name='time' style={{fontSize: 20}}/><Text>{'\u00A0'}{unserialize(data.jam).join(', ')}</Text>
                                    </View>
                                </Body>
                            </CardItem>
                        </Card>
                    </TouchableOpacity>
                )
            }else if(data.status == 'checkin') {
                return (
                    <TouchableOpacity onPress={() => {
                        const historyUser = {
                            tiket: data.tiket,
                            jam: data.jam,
                            tanggal: data.tanggal
                        }
                        AsyncStorage.setItem('getHistoryItem', JSON.stringify(historyUser))
                        this.props.navigation.navigate('Tiket')
                    }}>
                        <Card>
                            <CardItem>
                                <Thumbnail source={require('./assets/images/skyline.png')} />
                                <Body style={{marginLeft: '10%', marginTop: '2%'}}>
                                    <Badge success><Text style={{color: '#fff'}}>{data.status}</Text></Badge>
                                    <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                        <Icon name='time' style={{fontSize: 20}}/><Text>{'\u00A0'}{unserialize(data.jam).join(', ')}</Text>
                                    </View>
                                </Body>
                            </CardItem>
                        </Card>
                    </TouchableOpacity>
                )
            }
        })

        return forHistory
    }

    render() {
        return(
            <Container style={{backgroundColor: '#dfe6e9'}}>
                <Content style={{margin: '3%'}}>
                    <Grid>
                        <Row>
                            <Col>
                                {this.historyData()}
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}