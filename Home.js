import React from "react"
import { View, AsyncStorage, Text, Image } from "react-native"
import { StackActions, NavigationActions } from 'react-navigation'
import { Grid, Row, Col } from 'react-native-easy-grid'
import {
    Container,
    Content,
    Card,
    CardItem,
    Body,
    Button,
    Right,
    Thumbnail,
    Icon,
    Spinner,
    Badge
} from 'native-base'
import Axios from "axios";
import { urlApi } from './utils/Server'
import { unserialize } from 'locutus/php/var';
import { ScrollView } from "react-native-gesture-handler";
import { Card as Info, CardContent } from 'react-native-material-cards';
import Hr from 'react-native-hr-plus';

export default class Home extends React.Component {
    static navigationOptions = {
        title: 'Home',
        headerTitleStyle: {
            textAlign: 'center',
            flex: 1,
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            employee: {},
            loading: false,
            today: []
        }
    }

    async componentDidMount() {        

        await AsyncStorage.getItem('employee')
            .then(data => {
                this.setState({employee: JSON.parse(data)})
            }).catch(error => {
                console.log(error)
            })
        
        this.setState({loading: !this.state.loading})
        Axios.get(urlApi+'/information/highlight')
            .then(response => {
                this.setState({
                    today: response.data.today,
                    loading: false
                })
            })
    }

    getDateNow() {
        var now = new Date(date),
            month = '' + (now.getMonth() + 1),
            day = '' + (now.getDate() + 2),
            year = now.getFullYear();
        if (month.length < 2) month = '0' + month
        if (day.length < 2) day = '0' + day

        return [year, month, day].join('/');
    }

    todayRender() {
        const todayMap = this.state.today.map((item) => {
            const jam = unserialize(item.jam).toString()
            console.log(item)
            let status;
            if(item.status == 'pending') {
                status = <Badge warning><Text>Pending</Text></Badge>
            }else if(item.status == 'checkin'){
                status = <Badge success><Text>Checkin</Text></Badge>
            }
            return(
                <CardItem>
                    <Thumbnail style={{width: 32, height: 32}} source={require('./assets/images/skyline.png')} />
                    <Text>{item.name}{'\u00A0'}</Text>{status}
                    <Right>
                        <Text><Icon style={{color: '#000'}} name='time' />: {jam}</Text>
                    </Right>
                </CardItem>
            )
        })
        if(this.state.loading == true) {
            return(
                <CardItem style={{flexDirection: "row", justifyContent: "center"}}>
                    <Spinner color='green' />
                </CardItem>
            )
        } else if(this.state.today.length == 0) {
            return(
                <CardItem>
                    <Body>
                        <Text>No Schedule</Text>
                    </Body>
                </CardItem>
            )
        } else {
            return todayMap
        }
    }

    render() {
        return(
            <Container style={{backgroundColor: '#dfe6e9'}}>
                <Content style={{margin: '3%'}}>
                    <Card style={{ elevation: 2}}>
                        <CardItem>
                            <Thumbnail source={require('./assets/images/robot-dev.png')}/>
                            <Body style={{marginLeft: '10%', marginTop: '2%'}}>
                                <Text>{this.state.employee.name}</Text>
                                <Text>{this.state.employee.division} - {this.state.employee.position}</Text>
                            </Body>
                        </CardItem>
                    </Card>
                    <Text>{'\n'}</Text>
                    <ScrollView horizontal={true} style={{paddingBottom: 0}}>
                        <Info mediaSource={require('./assets/images/ied.jpg')} style={{width: '30%', height: '90%'}}>
                            <CardContent text="Idul Fitri" />
                        </Info>
                        <Info mediaSource={require('./assets/images/ied.jpg')} style={{width: '30%', height: '90%'}}>
                            <CardContent text="Idul Fitri" />
                        </Info>
                        <Info mediaSource={require('./assets/images/ied.jpg')} style={{width: '30%', height: '90%'}}>
                            <CardContent text="Idul Fitri" />
                        </Info>
                        <Info mediaSource={require('./assets/images/ied.jpg')} style={{width: '30%', height: '90%'}}>
                            <CardContent text="Idul Fitri" />
                        </Info>
                        <Info mediaSource={require('./assets/images/ied.jpg')} style={{width: '30%', height: '90%'}}>
                            <CardContent text="Idul Fitri" />
                        </Info>
                        <Info mediaSource={require('./assets/images/ied.jpg')} style={{width: '30%', height: '90%'}}>
                            <CardContent text="Idul Fitri" />
                        </Info>
                    </ScrollView>
                    <Text>{'\n'}</Text>
                    <Hr color="#95a5a6" width={1}>
                        <Text>{'\u00A0'}</Text><Icon name='information-circle-outline' style={{fontSize: 20, color: '#95a5a6'}} /><Text style={{color: '#95a5a6'}}>{'\u00A0'}Information {'\u00A0'}</Text>
                    </Hr>
                    <Text>{'\n'}</Text>
                    <Card style={{maxHeight: 300, minHeight: 300}}>
                        <CardItem header style={{flexDirection: "row", justifyContent: "center"}}>
                            <Text style={{alignItems: 'center', fontWeight: 'bold'}}>Today Schedule</Text>
                        </CardItem>
                        <ScrollView>
                            {this.todayRender()}
                        </ScrollView>
                        <CardItem footer style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                            <Text style={{color: '#95a5a6'}} onPress={() => console.log('test')}>See all {'\u00A0'}</Text><Icon name='arrow-round-forward' style={{fontSize: 20, color: '#95a5a6'}}/>
                        </CardItem>
                    </Card>
                </Content>
                <Button onPress={() => {
                    AsyncStorage.removeItem('employee')
                    this.props.navigation.dispatch(StackActions.reset({index: 0, actions: [NavigationActions.navigate({routeName: 'Login'})]}))
                }}><Text>Logout</Text></Button>
            </Container>
        )
    }
}